class RPNCalculator
  # attr_reader :value

  def initialize
    @stack = []
  end

  def push(num)
    @stack.push(num)
  end

  def value
    raise 'calculator is empty' if @stack.size.zero?
    @stack[-1]
  end

  def plus
    raise 'calculator is empty' if @stack.size < 2
    @stack[-2] += @stack[-1]
    @stack.pop
  end

  def minus
    raise 'calculator is empty' if @stack.size < 2
    @stack[-2] -= @stack[-1]
    @stack.pop
  end

  def times
    raise 'calculator is empty' if @stack.size < 2
    @stack[-2] *= @stack[-1]
    @stack.pop
  end

  def divide
    raise 'calculator is empty' if @stack.size < 2
    @stack[-2] = @stack[-2].fdiv(@stack[-1])
    @stack.pop
  end

  def tokens(string)
    operands = { '+' => :+,
                 '-' => :-,
                 '*' => :*,
                 '/' => :/
    }
    string.split(' ').map { |x| (operands.key? x) ? operands[x] : x.to_i }
  end

  def evaluate(string)
    operands = { '+' => -> { plus },
                 '-' => -> { minus },
                 '*' => -> { times },
                 '/' => -> { divide }
    }
    list = string.split(' ').map { |x| (operands.key? x) ? x : x.to_i }
    list.each { |x| (x.is_a? Integer) ? push(x) : operands[x].call }
    value
  end
end
